-- additional buffs list (still quite not up-to-date with 8.x)

local MY_ADDITIONAL_BUFFS = {
  "Cultivation",
  "Spring Blossoms",
  "Grove Tending"
}

BiggerBuffs_CooldownsData = {
  MY_ADDITIONAL_BUFFS = MY_ADDITIONAL_BUFFS
}
